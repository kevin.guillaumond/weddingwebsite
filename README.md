# README

Looks like I'm getting married, so let's put together a website :)

## Tech stack
The website is built with Ruby 2.4.2 and Rails 5.1.6

## Running the tests
You need PostgreSQL to be able to run the tests.

Once you have that, you just need to create a user and copy their username and
password in `config/database.yml`

Run the tests with `bundle exec rspec`

## Running the website locally
Once you have Ruby and Rails installed, you just need to set `secret_key_base`
for the development environment in `config/secrets.yml` to a 30 characters
  random string.

Alternatively, you can forget about that file and just set the
`SECRET_KEY_BASE_DEV` environment variable instead.

`rails s` should do the rest, and make the website available at
`localhost:3000`.

### Email

I use [MailCatcher](https://mailcatcher.me/) to intercept email.

Install MailCatcher with `gem install mailcatcher` and run it with
`mailcatcher`.

The emails sent by the application can be seen at `http://localhost:1080/`

## Deployment

### Staging

Every commit to all the branches except master deploys the website
automatically to the staging environment.

Staging is hosted on [Heroku](https://radiant-mesa-69627.herokuapp.com/) and is
available at http://staging.eatdrinkbemarried.guillaumond.me/.

Status of the develop branch: [![pipeline
status](https://gitlab.com/kevin.guillaumond/weddingwebsite/badges/develop/pipeline.svg)](https://gitlab.com/kevin.guillaumond/weddingwebsite/commits/develop)

### Production
Every commit to master deploys the website automatically to the production
environment.

Production is also hosted on
[Heroku](https://peaceful-lake-92649.herokuapp.com/) and is available at
http://eatdrinkbemarried.guillaumond.me/.

Status of the master branch: [![pipeline
status](https://gitlab.com/kevin.guillaumond/weddingwebsite/badges/master/pipeline.svg)](https://gitlab.com/kevin.guillaumond/weddingwebsite/commits/master)
