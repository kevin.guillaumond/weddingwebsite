module GuestsHelper
  def language_options(guest)
    selected_option = guest.language.nil? ? 'en' : guest.language
    # TODO: Get options from locales file
    options_for_select(%w[fr en], selected_option)
  end

  def rsvp_options(guest)
    selected_option = guest.rsvp.nil? ? 'unknown' : guest.rsvp
    # TODO: Get options from Guest model
    options_for_select(%w[unknown yes no], selected_option)
  end

  def partner_options(guest)
    sorted_single_guests = Guest.all.reject(&:has_partner?).sort_by(&:last_name)

    guests_to_display = get_partner_dropdown(guest, sorted_single_guests)

    all_guest_options = guests_to_display.map do |g|
      ["#{g.first_name} #{g.last_name}", g.id]
    end

    all_options = [['Unknown', 0]] + all_guest_options
    options_for_select(all_options, guest.partner_id)
  end

  private

    def get_partner_dropdown(guest, sorted_single_guests)
      if !guest.partner_id.nil? && guest.partner_id > 0
        current_partner = Guest.find(guest.partner_id)
        guests_to_display = [current_partner] + sorted_single_guests
      else
        guests_to_display = sorted_single_guests
      end
      guests_to_display
    end
end
