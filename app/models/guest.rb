class Guest < ApplicationRecord
  enum rsvp: %i[unknown yes no]
end
