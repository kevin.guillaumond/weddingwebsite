class GuestsController < ApplicationController
  before_action :require_login

  def index
    @guests = Guest.all.order(:last_name)
  end

  def new
    @guest = Guest.new
  end

  def create
    @guest = Guest.new(guest_params)
    if @guest.save
      # TODO: show success flash (#40)
      update_guest_partner(@guest)
      redirect_to guests_path
    else
      render 'new'
    end
  end

  def edit
    @guest = Guest.find(params[:id])
  end

  def update
    @guest = Guest.find(params[:id])
    # TODO: this should be one transaction
    remove_partner_from_guest(@guest.partner_id) if guest_params[:partner_id] != @guest.partner_id

    if @guest.update_attributes(guest_params)
      # TODO: show success flash (#40)
      update_guest_partner(@guest)
      redirect_to guests_path
    else
      render 'edit'
    end
  end

  def email_all
    # TODO: get name of email as param

    Guest.all.each do |guest|
      I18n.with_locale(guest.language) do
        GuestMailer.with(guest: guest).save_the_date.deliver_later
      end
    end
  end

  private

    # Accepting only safe parameters in the POST/PATCH requests
    def guest_params
      guest_params = params.require(:guest)
                           .permit(:first_name, :last_name, :email, :language,
                                   :rsvp, :has_partner, :partner_id, :notes)

      if guest_params[:has_partner] == 'false'
        guest_params[:partner_id] = '0'
        # TODO: Add a string to be displayed in a warning flash (#40) if ID wasn't
        # 0 previously
      end
      guest_params
    end

    def update_guest_partner(guest)
      return if !guest.has_partner || guest.partner_id.zero?

      partner = Guest.find(guest.partner_id)

      if partner.has_partner?
        # Error - can't choose as partner somebody who already has one
        # Shouldn't happen from the UI
      else
        # TODO: Error handling when partner can't be found or updated
        partner.update(has_partner: true, partner_id: guest.id) unless partner.nil?
      end
    end

    def remove_partner_from_guest(guest_id)
      return if guest_id.zero?

      guest = Guest.find(guest_id)
      guest.update(has_partner: false, partner_id: 0)
    end
end
