# frozen_string_literal: true

class StaticPagesController < ApplicationController
  def home; end

  def venue; end

  def schedule; end

  def under_construction; end
end
