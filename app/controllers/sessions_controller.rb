# frozen_string_literal: true

class SessionsController < Clearance::SessionsController
  # Default behavior - just added remember_token cookie
  # https://github.com/thoughtbot/clearance/blob/53107d90d37d238ee59a7297aa564027ce63c39a/app/controllers/clearance/sessions_controller.rb#L5-L16
  def create
    @user = authenticate(params)

    sign_in(@user) do |status|
      if status.success?
        # Not sure why this isn't the case by default...
        # https://stackoverflow.com/a/9545814/3898322
        cookies['remember_token'] = @user.remember_token
        redirect_back_or url_after_create
      else
        flash.now.alert = status.failure_message
        render template: 'sessions/new', status: :unauthorized
      end
    end
  end

  def url_after_destroy
    root_url
  end
end
