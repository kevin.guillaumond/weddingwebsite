class GuestMailer < ApplicationMailer
  default from: 'eatdrinkbemarried@guillaumond.me'

  def save_the_date
    @guest = params[:guest]
    mail(to: @guest.email)
  end
end
