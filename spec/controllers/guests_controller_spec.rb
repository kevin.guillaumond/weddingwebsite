# frozen_string_literal: true

require 'rails_helper'

RSpec.describe GuestsController, type: :controller do
  describe 'Show list of guests' do
    context 'Not signed in' do
      it 'redirects to sign in page' do
        get :index

        # TODO, probably not the best way to write it
        expect(response).to redirect_to('/en/sign_in')
      end
    end

    context 'Signed in' do
      before(:each) do
        sign_in
      end

      it 'renders the index template' do
        get :index

        expect(response).to render_template('index')
        assert_response 200
      end

      it 'shows the guests ordered by last name' do
        guest_g = create(:guest, last_name: 'Guillaumond')
        guest_a = create(:guest, last_name: 'Adams')
        guest_x = create(:guest, last_name: 'Xu')

        get :index

        expect(assigns(:guests)).to eq([guest_a, guest_g, guest_x])
      end
    end
  end

  describe 'Create new guest' do
    context 'Not signed in' do
      it 'redirects to sign in page' do
        post :create

        expect(response).to redirect_to('/en/sign_in')
      end
    end

    context 'Signed in' do
      before(:each) do
        sign_in
      end

      def valid_guest_params
          { guest: { first_name: 'Anna',
                     last_name: 'Smith',
                     email: 'anna@smith.com',
                     language: 'fr',
                     rsvp: 'yes',
                     has_partner: true,
                     partner_id: 0 } }
      end

      it 'creates a guest if all fields are correct' do
        expect do
          post :create, params: valid_guest_params
        end.to change { Guest.count }.from(0).to(1)
        expect(response).to redirect_to(guests_path)
      end

      it 'resets partner ID if there is no partner' do
        params = valid_guest_params
        params[:guest][:has_partner] = false
        params[:guest][:partner_id] = 25

        expect do
          post :create, params: params
        end.to change { Guest.count }.from(0).to(1)

        new_guest = Guest.first
        expect(new_guest.has_partner).to eq(false)
        expect(new_guest.partner_id).to eq(0)
      end

      it "updates partner if new guest is somedoby's partner" do
        existing_guest = create(:guest, has_partner: false, partner_id: 0)

        params = valid_guest_params
        params[:guest][:has_partner] = true
        params[:guest][:partner_id] = existing_guest.id

        expect do
          post :create, params: params
        end.to change { Guest.count }.from(1).to(2)

        new_guest = Guest.find_by(email: params[:guest][:email])
        expect(new_guest.has_partner).to eq(true)
        expect(new_guest.partner_id).to eq(existing_guest.id)
        existing_guest.reload
        expect(existing_guest.has_partner).to eq(true)
        expect(existing_guest.partner_id).to eq(new_guest.id)
      end
    end
  end

  describe 'Show page to edit guest' do
    before(:each) do
      @guest = create(:guest)
    end

    context 'Not signed in' do
      it 'redirects to sign in page' do
        get :edit, params: { id: @guest.id }

        expect(response).to redirect_to('/en/sign_in')
      end
    end

    context 'Signed in' do
      before(:each) do
        sign_in
      end

      it 'renders edit template' do
        get :edit, params: { id: @guest.id }

        expect(response).to render_template(:edit)
      end
    end
  end

  describe 'Edit guest' do
    before(:each) do
      @guest = create(:guest)
    end

    context 'Not signed in' do
      it 'redirects to sign in page' do
        patch :update, params: { id: @guest.id }

        expect(response).to redirect_to('/en/sign_in')
      end
    end

    context 'Signed in' do
      before(:each) do
        sign_in
      end

      it 'updates guest' do
        new_email = 'this.is@new.email'

        patch :update, params: { id: @guest.id, guest: { email: new_email } }

        updated_guest = Guest.find(@guest.id)
        expect(updated_guest.email).to eq(new_email)
        expect(response).to redirect_to(guests_path)
      end

      it 'resets partner ID if there is no partner' do
        patch :update, params: {
          id: @guest.id,
          guest: {
            has_partner: false,
            partner_id: 123
          }
        }

        @guest.reload
        expect(@guest.has_partner).to eq(false)
        expect(@guest.partner_id).to eq(0)
      end

      it "updates old and new partner if guest's partner info is updated" do
        # Register guest with old_partner
        old_partner = create(:guest, has_partner: true, partner_id: 0)
        guest = create(:guest, has_partner: true, partner_id: old_partner.id)
        old_partner.update(partner_id: guest.id)

        new_partner = create(:guest, has_partner: false, partner_id: 0)

        # Update guest with new partner info
        patch :update, params: {
          id: guest.id,
          guest: {
            has_partner: true,
            partner_id: new_partner.id
          }
        }

        old_partner.reload
        expect(old_partner.has_partner).to eq(false)
        expect(old_partner.partner_id).to eq(0)

        guest.reload
        expect(guest.has_partner).to eq(true)
        expect(guest.partner_id).to eq(new_partner.id)

        new_partner.reload
        expect(new_partner.has_partner).to eq(true)
        expect(new_partner.partner_id).to eq(guest.id)
      end
    end
  end

  describe 'Send email to guests' do
    context 'Not signed in' do
      it 'redirects to sign in page' do
        post :email_all

        expect(response).to redirect_to('/en/sign_in')
      end
    end

    context 'Signed in' do
      before(:each) do
        sign_in
      end

      xit 'sends a save the date email to every guest' do
        # TODO: #46
        guest = create(:guest)
        guest_mailer = class_double('GuestMailer')
        mail = double(:mail)

        expect(GuestMailer).to receive(:with).with(guest).and_return(guest_mailer)
        expect(guest_mailer).to receive(:save_the_date).and_return(mail)
        expect(mail).to receive(:deliver_later)
      end

      xit "sends the email in the user's language" do
        # TODO: #46
      end
    end
  end
end
