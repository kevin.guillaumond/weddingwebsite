# frozen_string_literal: true

require 'rails_helper'

RSpec.describe StaticPagesController, type: :controller do
  describe 'GET #home' do
    it 'successfully renders the home view' do
      get :home

      expect(response).to render_template('home')
      assert_response 200
    end
  end
end
