# frozen_string_literal: true

FactoryBot.define do
  factory :user do
    email { 'user@guillaumond.me' }
    password { 'correcthorsebatterystaple' }
  end
end
