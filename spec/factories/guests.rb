FactoryBot.define do
  factory :guest do
    first_name { 'John' }
    last_name { 'Doe' }
    email { 'john.doe@example.com' }
    language { 'en' }
    rsvp { 1 }
    has_partner { false }
    partner_id { 0 }
  end
end
