require 'rails_helper'

RSpec.describe 'guests/_form' do
  context 'no guest provided' do
    before(:each) do
      @guest = Guest.new
    end

    it "fills form's default values" do
      render

      expect(response).to have_selector('select#guest_language')
      expect(response).to have_select('Language', options: %w[en fr], selected: 'en')

      expect(response).to have_selector('select#guest_rsvp')
      expect(response).to have_select('Rsvp', options: %w[unknown yes no], selected: 'unknown')
    end
  end

  context 'editing guest' do
    before(:each) do
      @guest = build(:guest, language: 'fr', rsvp: 'yes')
    end

    it "fills form with guest's values" do
      render

      expect(response).to have_field('First name', with: @guest.first_name)
      expect(response).to have_field('Last name', with: @guest.last_name)
      expect(response).to have_field('Email', with: @guest.email)

      expect(response).to have_selector('select#guest_language')
      expect(response).to have_select('Language', options: %w[en fr], selected: 'fr')

      expect(response).to have_selector('select#guest_rsvp')
      expect(response).to have_select('Rsvp', options: %w[unknown yes no], selected: 'yes')
    end
  end
end
