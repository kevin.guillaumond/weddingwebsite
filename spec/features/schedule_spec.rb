# frozen_string_literal: true

require 'rails_helper'

RSpec.feature 'Schedule page', type: :feature do
  scenario 'User visits English schedule page' do
    visit '/en/schedule'

    expect(page).to have_title("Schedule | Kathy and Kevin's wedding")
    expect(page).to have_text('not finalized yet')
  end

  scenario 'User visits French schedule page' do
    visit '/fr/schedule'

    expect(page).to have_title('Planning | Mariage de Kathy et Kevin')
    expect(page).to have_text('pas encore terminé')
  end
end
