# frozen_string_literal: true

require 'rails_helper'

RSpec.feature 'Home page', type: :feature do
  scenario 'User visits English home' do
    visit '/en/home'

    expect(page).to have_title("Home | Kathy and Kevin's wedding")
    expect(page).to have_text('She said yes!')
  end

  scenario 'User visits French home' do
    visit '/fr/home'

    expect(page).to have_title('Accueil | Mariage de Kathy et Kevin')
    expect(page).to have_text('Elle a dit oui!')
  end
end
