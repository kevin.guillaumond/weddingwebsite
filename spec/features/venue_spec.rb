# frozen_string_literal: true

require 'rails_helper'

RSpec.feature 'Venue page', type: :feature do
  scenario 'User visits English venue page' do
    visit '/en/venue'

    expect(page).to have_title("Venue | Kathy and Kevin's wedding")
    expect(page).to have_text('Switzerland')
  end

  scenario 'User visits French venue page' do
    visit '/fr/venue'

    expect(page).to have_title('Lieu | Mariage de Kathy et Kevin')
    expect(page).to have_text('Suisse')
  end
end
