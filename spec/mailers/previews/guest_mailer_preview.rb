# Preview all emails at http://localhost:3000/rails/mailers/guest_mailer
class GuestMailerPreview < ActionMailer::Preview
  def save_the_date
    GuestMailer.with(guest: Guest.first).save_the_date
  end
end
