Clearance.configure do |config|
  config.allow_sign_up = false  # User will be added to the DB manually
  config.cookie_domain = '.guillaumond.me'
  config.mailer_sender = 'reply@guillaumond.me'
  config.rotate_csrf_on_sign_in = true
  config.routes = false # Recommended by the docs
end
