# frozen_string_literal: true

Rails.application.routes.draw do
  scope '(:locale)', locale: /#{I18n.available_locales.join('|')}/ do
    root 'static_pages#home'

    get '/home' => 'static_pages#home'
    get '/venue' => 'static_pages#venue'
    get '/schedule' => 'static_pages#schedule'
    get '/under_construction' => 'static_pages#under_construction'

    resources :guests

    post '/guests/email_all' => 'guests#email_all'

    # Clearance routes
    resources :passwords,
      controller: 'clearance/passwords',
      only: [:create, :new]

    resource :session,
      controller: 'sessions',
      only: [:create] 

    resources :users,
      controller: 'clearance/users',
      only: Clearance.configuration.user_actions do
        resource :password,
        controller: 'clearance/passwords',
        only: [:create, :edit, :update]
      end

    get '/sign_in' => 'sessions#new', as: 'sign_in'
    delete '/sign_out' => 'sessions#destroy', as: 'sign_out'

  end

  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
