class AddPartnerToGuests < ActiveRecord::Migration[5.1]
  def change
    add_column :guests, :has_partner, :boolean
    add_column :guests, :partner_id, :int
  end
end
