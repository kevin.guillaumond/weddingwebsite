class AddNotesToGuests < ActiveRecord::Migration[5.1]
  def change
    add_column :guests, :notes, :text
  end
end
